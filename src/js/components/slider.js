import 'swiper/dist/css/swiper.min.css';
import {
  Swiper,
  Navigation,
  Pagination,
  Autoplay,
  Parallax,
} from 'swiper/dist/js/swiper.esm';

Swiper.use([Navigation, Pagination, Autoplay, Parallax]);

const sliderEl = document.querySelector('.js-hero-slider');
const header = document.querySelector('.js-page-header');

if (sliderEl) {
  const args = {
    speed: 800,
    loop: true,
    // autoplay: {
    //   delay: 5000,
    // },
    parallax: true,
  };
  const next = sliderEl.querySelector('.js-next');
  const prev = sliderEl.querySelector('.js-prev');
  if (next && prev) {
    args.navigation = {
      nextEl: next,
      prevEl: prev,
    };
  }
  sliderEl.slider = new Swiper(sliderEl, args);

  sliderEl.slider.on('slideChange', () => {
    const { color } = sliderEl.slider.slides[sliderEl.slider.realIndex + 1].dataset;
    if (color) {
      header.style.color = color;
    } else {
      header.style.color = 'inherit';
    }
  });
}
